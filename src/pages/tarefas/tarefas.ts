import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'tarefas.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

}
