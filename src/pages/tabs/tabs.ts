import { Component } from '@angular/core';

import { AboutPage } from '../projetos/projetos';
import { ContactPage } from '../notificacoes/notificacoes';
import { HomePage } from '../tarefas/tarefas';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;

  constructor() {

  }
}
