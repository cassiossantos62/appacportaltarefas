import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Registrar} from "../registrar/registrar";



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
    console.log('Hellow login Page');
  }

  doRegister(){
    this.navCtrl.push(Registrar);
  }
}
