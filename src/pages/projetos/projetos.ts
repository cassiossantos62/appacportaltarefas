import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'projetos.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController) {

  }

}
