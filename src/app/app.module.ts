import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/projetos/projetos';
import { ContactPage } from '../pages/notificacoes/notificacoes';
import { HomePage } from '../pages/tarefas/tarefas';
import { TabsPage } from '../pages/tabs/tabs';
import { Login } from '../pages/login/login';
import { Registrar } from '../pages/registrar/registrar';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {LoginProvider} from "../providers/login-provider";
import firebase from "firebase";

//Login com FIREBASE
const firebaseConfig = {
  apiKey: "AIzaSyDmSsZAO31K2hxlnp-vxsSqnsUkPxlnYjo",
  authDomain: "app-acportal-tarefas.firebaseapp.com",
  databaseURL: "https://app-acportal-tarefas.firebaseio.com",
  projectId: "app-acportal-tarefas",
  storageBucket: "app-acportal-tarefas.appspot.com",
  messagingSenderId: "708648840352"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Login,
    Registrar
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Login,
    Registrar
  ],
  providers: [
    StatusBar,
    LoginProvider,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
  constructor(){
    //Inicializando FIREBASE
    firebase.initializeApp(firebaseConfig);
  }
}
