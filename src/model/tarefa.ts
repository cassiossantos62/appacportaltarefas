import{TarefaEstado} from "./tarefa-estado";

export class Tarefa {
    codigo: number;
    titulo: string;
    descricao?: string;
    state:TarefaEstado;
}